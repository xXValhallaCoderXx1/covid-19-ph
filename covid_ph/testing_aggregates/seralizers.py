from rest_framework import serializers

from .models import TestingAggregate

# Readonly - Never created or updated by serializer


class TestingAggregateSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestingAggregate
        fields = "__all__"
        read_only_fields = (
            "id",
            "created_date",
            "updated_date",
        )
