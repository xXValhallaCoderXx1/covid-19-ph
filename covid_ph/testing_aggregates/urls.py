# from django.urls import path

# from .views import TestingAggregateDetail, TestingAggregateList
from rest_framework import routers

from .views import TestingAggregateViewSet

router = routers.DefaultRouter()
router.register(r"testing_aggregates", TestingAggregateViewSet)
