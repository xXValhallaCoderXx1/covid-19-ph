from django.contrib import admin

from .models import TestingAggregate


@admin.register(TestingAggregate)
class TestingAggregateAdmin(admin.ModelAdmin):
    fields = (
        "facility_name",
        "created_date",
        "updated_date",
    )
    list_display = (
        "facility_name",
        "created_date",
        "updated_date",
    )
    readonly_fields = (
        "created_date",
        "updated_date",
    )
