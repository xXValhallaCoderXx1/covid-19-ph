from django_filters import rest_framework as filters

from .models import TestingAggregate


class TestingAggregateFilter(filters.FilterSet):
    facility_name = filters.CharFilter(field_name="facility_name")

    class Meta:
        model = TestingAggregate
        fields = "__all__"
