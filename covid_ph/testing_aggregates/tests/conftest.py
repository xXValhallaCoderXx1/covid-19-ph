# import os

# from django.conf import settings

# import pytest

# """
# Fixtures are reusable objects for tests. They have a scope associated with them,
# which indicates how often the fixture is invoked:

# function - once per test function (default)
# class - once per test class
# module - once per test module
# session - once per test session

# """

# DEFAULT_ENGINE = "django.db.backends.postgresql_psycopg2"


# @pytest.fixture(scope="session")
# def django_db_setup():
#     settings.DATABASES["default"] = {
#         "ENGINE": os.environ.get("DB_TEST_ENGINE", DEFAULT_ENGINE),
#         "HOST": os.environ["DB_TEST_HOST"],
#         "NAME": os.environ["DB_TEST_NAME"],
#         "PORT": os.environ["DB_TEST_PORT"],
#         "USER": os.environ["DB_TEST_USER"],
#         "PASSWORD": os.environ["DB_TEST_PASSWORD"],
#     }


import pytest

from testing_aggregates.models import TestingAggregate


@pytest.fixture(scope="function")
def add_testing_aggregate():
    def _add_testing_aggregate(facility_name):
        testing_aggregate = TestingAggregate.objects.create(facility_name=facility_name)
        return testing_aggregate

    return _add_testing_aggregate
