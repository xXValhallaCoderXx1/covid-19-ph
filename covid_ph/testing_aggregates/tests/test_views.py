import pytest
from testing_aggregates.models import TestingAggregate


@pytest.mark.django_db
def test_add_testing_aggregate(client):
    testing_aggregates = TestingAggregate.objects.all()
    assert len(testing_aggregates) == 0

    resp = client.post(
        "/api/v1/testing_aggregates/",
        {"facility_name": "Makati Med"},
        content_type="application/json",
    )

    assert resp.status_code == 201
    assert resp.data["facility_name"] == "Makati Med"
    testing_aggregates = TestingAggregate.objects.all()
    assert len(testing_aggregates) == 1


@pytest.mark.django_db
def test_add_testing_aggregate_invalid_json(client):
    testing_aggregates = TestingAggregate.objects.all()
    assert len(testing_aggregates) == 0

    resp = client.post(
        "/api/v1/testing_aggregates/", {}, content_type="application/json"
    )
    assert resp.status_code == 400

    testing_aggregates = TestingAggregate.objects.all()
    assert len(testing_aggregates) == 0


@pytest.mark.django_db
def test_add_testing_aggregate_invalid_json_keys(client):
    testing_aggregates = TestingAggregate.objects.all()
    assert len(testing_aggregates) == 0

    resp = client.post(
        "/api/v1/testing_aggregates/",
        {"facility": "The Big Lebowski"},
        content_type="application/json",
    )
    assert resp.status_code == 400

    testing_aggregates = TestingAggregate.objects.all()
    assert len(testing_aggregates) == 0


@pytest.mark.django_db
def test_get_testing_aggregate(client, add_testing_aggregate):
    testing_aggregate = add_testing_aggregate(facility_name="Makati Med")
    resp = client.get(f"/api/v1/testing_aggregates/{testing_aggregate.id}/")
    assert resp.status_code == 200
    assert resp.data["facility_name"] == "Makati Med"


def test_get_single_testing_aggregate_incorrect_id(client):
    resp = client.get("/api/v1/testing_aggregates/foo/")
    assert resp.status_code == 404


@pytest.mark.django_db
def test_get_all_testing_aggregate(client, add_testing_aggregate):
    aggregate_one = add_testing_aggregate(facility_name="Makati Med")
    aggregate_two = add_testing_aggregate("General Hospital")
    resp = client.get("/api/v1/testing_aggregates/")
    assert resp.status_code == 200
    assert resp.data[0]["facility_name"] == aggregate_one.facility_name
    assert resp.data[1]["facility_name"] == aggregate_two.facility_name
