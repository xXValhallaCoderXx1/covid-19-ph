import pytest

from testing_aggregates.models import TestingAggregate


@pytest.mark.django_db
def test_movie_model():
    testing_aggregate = TestingAggregate(facility_name="Makati Med")
    testing_aggregate.save()
    assert testing_aggregate.facility_name == "Makati Med"
    assert testing_aggregate.created_date
    assert testing_aggregate.updated_date
    assert str(testing_aggregate) == testing_aggregate.facility_name
