from testing_aggregates.seralizers import TestingAggregateSerializer


def test_valid_testing_aggregate_serializer():
    valid_serializer_data = {
        "facility_name": "Makati Med",
    }
    serializer = TestingAggregateSerializer(data=valid_serializer_data)
    assert serializer.is_valid()
    assert serializer.validated_data == valid_serializer_data
    assert serializer.data == valid_serializer_data
    assert serializer.errors == {}


def test_invalid_testing_aggregate_serializer():
    invalid_serializer_data = {}
    serializer = TestingAggregateSerializer(data=invalid_serializer_data)
    assert not serializer.is_valid()
    assert serializer.validated_data == {}
    assert serializer.data == invalid_serializer_data
    assert serializer.errors == {"facility_name": ["This field is required."]}
