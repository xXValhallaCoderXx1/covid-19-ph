from django.apps import AppConfig


class TestingAggregatesConfig(AppConfig):
    name = "testing_aggregates"
