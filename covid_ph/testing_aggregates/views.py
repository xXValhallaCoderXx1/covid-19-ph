# from django.http import Http404, JsonResponse
# from rest_framework import status
# from rest_framework.response import Response
# from rest_framework.views import APIView
from django_filters import rest_framework as filters
from rest_framework import viewsets

from .filters import TestingAggregateFilter
from .models import TestingAggregate
from .seralizers import TestingAggregateSerializer

# def ping(request):
#     data = {"ping": "pong"}
#     return JsonResponse(data)


class TestingAggregateViewSet(viewsets.ModelViewSet):
    queryset = TestingAggregate.objects.all()
    serializer_class = TestingAggregateSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = TestingAggregateFilter


# class TestingAggregateList(APIView):
#     def get(self, request, format=None):
#         testing_aggregates = TestingAggregate.objects.all()
#         serializer = TestingAggregateSerializer(testing_aggregates, many=True)
#         return Response(serializer.data)

#     def post(self, request, format=None):
#         serializer = TestingAggregateSerializer(data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data, status=status.HTTP_201_CREATED)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# class TestingAggregateDetail(APIView):
#     def get_object(self, pk):
#         try:
#             return TestingAggregate.objects.get(pk=pk)
#         except TestingAggregate.DoesNotExist:
#             raise Http404

#     def get(self, request, pk, format=None):
#         testing_aggregate = self.get_object(pk)
#         serializer = TestingAggregateSerializer(testing_aggregate)
#         return Response(serializer.data)
