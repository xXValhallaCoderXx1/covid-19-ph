import os
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = "Creates a superuser if it doesnt exist"

    # def add_arguments(self, parser):
    #     parser.add_argument("--username", help="Admin Username")
    #     parser.add_argument("--email", help="Add Email")
    #     parser.add_argument("--password", help="Add Password")

    def handle(self, *args, **options):
        DEBUG = int(os.environ.get("DEBUG", default=0))
        USERNAME = os.environ.get("ADMIN_USERNAME")
        EMAIL = os.environ.get("ADMIN_EMAIL")
        PASSWORD = os.environ.get("ADMIN_PASSWORD")
        username = USERNAME if DEBUG == 0 else "admin"
        email = EMAIL if DEBUG == 0 else "admin@admin.com"
        password = PASSWORD if DEBUG == 0 else "123456"
        User = get_user_model()
        ENV_MODE = "production" if DEBUG == 0 else "development"
        print(ENV_MODE)
        if not User.objects.filter(is_superuser=True).exists():
            User.objects.create_superuser(
                username=username, email=email, password=password
            )
        else:
            print("SUPERUSER EXISTS")
